/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * This is the main application class of your custom application "${Name}"
 *
 * @asset(ifungi/*)
 */
qx.Class.define("ifungi.Application",
{
  extend : qx.application.Mobile,

  members :
  {

    /**
     * This method contains the initial application code and gets called
     * during startup of the application
     */
    main : function() {
      // Call super class
      this.base(arguments);

      // Enable logging in debug variant
      if (qx.core.Environment.get("qx.debug")) {
        // support native logging capabilities, e.g. Firebug for Firefox
        qx.log.appender.Native;
        // support additional cross-browser console.
        // Trigger a "longtap" event on the navigation bar for opening it.
        qx.log.appender.Console;
      }

      /*
      -------------------------------------------------------------------------
        Below is your actual application code...
        Remove or edit the following code to create your application.
      -------------------------------------------------------------------------
      */

      qx.module.Css.includeStylesheet("resource/ifungi/css/style.css");

      // Instance of singleton class Arbre
      var arbre = ifungi.data.Arbre.getInstance();
      // Instance of singleton class BD
      var bd = ifungi.data.BD.getInstance();
      // Navigation Pages
      var menu = new ifungi.page.Menu();
      var voidpage = new ifungi.page.Void();
      var critere = new ifungi.page.Critere();
      var result = new ifungi.page.Result();
      var description = new ifungi.page.DescriptionSpecie();
      var help = new ifungi.page.HelpCriteria();
      var overview = new ifungi.page.Overview();

      // // Buton Help for showing information On criteria
      // var buttonHelpCriteria = new qx.ui.mobile.container.Composite();
      // buttonHelpCriteria.add(new qx.ui.mobile.basic.Image("resource/ifungi/helpCritere.png"));
      // voidpage.getLeftContainer().add(buttonHelpCriteria);
      // buttonHelpCriteria.addListener("tap", function(evt) {
      //   this.debug("Help critere");
      //   help.show();
      // }, this);

      // // Button for unchecked all checkboxes
      // var bouton = new qx.ui.mobile.form.Button("Tout décocher");
      // critere.add(bouton);
      // // addListener on button
      // bouton.addListener("tap", function(evt) {
      // }, this);

      // The back button name
      critere.setBackButtonText(this.tr("Menu"));
      voidpage.setBackButtonText(this.tr("Criteria"));
      result.setBackButtonText(this.tr("Criteria"));
      description.setBackButtonText(this.tr("Result"));
      overview.setBackButtonText(this.tr("Menu"));
      // Shown Back button on tablet
      // voidpage.setShowBackButtonOnTablet(true);
      critere.setShowBackButtonOnTablet(true);
      // result.setShowBackButtonOnTablet(true);
      description.setShowBackButtonOnTablet(true);
      // overview.setShowBackButtonOnTablet(true);
      help.setShowBackButtonOnTablet(true);

      // Add the pages to the page manager.
      var manager = new qx.ui.mobile.page.Manager();
      // manager.setHideMasterOnDetailStart(true);
      // manager.SetHideMasterOnDetailStart(true);
      // this.debug(manager.__isTablet);
      // this.debug(manager.getMasterButton());
      manager.setHideMasterButtonCaption(this.tr("Hide"));
      // this.debug(manager.getDetailContainer());
      // this.debug(manager.isHideMasterOnDetailStart());
      // this.debug(manager.getDetailNavigation());
      // this.debug(manager.isHideMasterOnDetailStart());
      // this.debug(manager.getMasterButton());

      manager.addMaster([
        menu,
        critere
      ]);
      manager.addDetail([
        voidpage,
        result,
        description,
        help,
        overview
      ]);

      // Initialize the applicacriteretion routing
      this.getRouting().onGet("/", function() {

        help.show({"animation": null});
        overview.show({"animation": null});
        result.show({"animation": null});
        description.show({"animation": null});

        voidpage.show({"animation": null});
        critere.show({"animation": null});
        menu.show({"animation": null});
      }, this);

      // Load the critere and show the critere page
      menu.addListener("tapMorphological", function(evt) {
        critere.show();
      }, this);

      // Load the overview and show the overview page
      menu.addListener("tapEducativeRes", function(evt) {
        // this.debug(evt.getData());
        overview.show();
      }, this);

      // Load Json file and show menu criteres
      arbre.addListener("loadedJsonFile", function(evt) {
        critere.showTree(evt.getData());
      }, this);

      var dicoCriteres = [];
      // Load the critere indexes and add to the critere dictionnary when critere checked
      critere.addListener("ajouterCritere", function(evt) {
        bd.getAllIndexesOfAllCriteres();
        // this.debug("Dico decriteres Criteres");
        this.debug(dicoCriteres);
        dicoCriteres.push(evt.getData());
        bd.rechercheEspeces(dicoCriteres);
      }, this);

      // Load the critere indexes and remove the critere unchecked in the critere dictionnary
      critere.addListener("enleverCritere", function(evt) {
        // this.debug(dicoCriteres);
        // this.debug(evt.getData());
        for (let i = 0; i < dicoCriteres.length; i++) {
          // this.debug(dicoCriteres[i]);
          if (JSON.stringify(dicoCriteres[i]) === JSON.stringify(evt.getData())) {
            // this.debug(i);
            dicoCriteres.splice(i, 1);
            // this.debug(dicoCriteres);
            // this.debug(dicoCriteres);
            bd.rechercheEspeces(dicoCriteres);
          }
        }
      }, this);

      // Geting all indexes of species in database and load
      // species names and specie images corresponding, in the result page
      bd.addListener("getSpeciesIndexesFinal", function(evt) {
        result.show();
        var indexSpeciesforResult = evt.getData();
        var allListOfSpecies = bd.getSpeciesList();
        var allImages = bd.getAllImages();
        /**
         * Management of the display of the images results according to
         * whether it is tablet or telephone or it's a landscape or portrait.
         * The images are resized at each orientation of the screen.
         * On tablet (portrait or landscape), the size of the image is
         * half the width of the window (less a certain margin).
         * On telephone, in protrait the size of the image coresponds to
         * the width of the window and in landscape it corresponds
         * to half of the width of the window (less a certain margin).
         */
        // Getting window first with before moving it
        var screenWidth = (window.screen.availWidth-(window.screen.availWidth*0.1));
        // result.showResultOnPhone(indexSpeciesforResult, allListOfSpecies, allImages, screenWidth);
        // this.debug(result._isTablet);
        // this.debug(window.screen.availWidth);
        // this.debug(window.screen.availHeight);
        if (result._isTablet == true) { // It's tablet
          result.showResults(indexSpeciesforResult, allListOfSpecies, allImages, screenWidth);
        } else { // It's not tablet
          if (window.screen.availWidth > window.screen.availHeight) { // It's Landscape
           result.showResults(indexSpeciesforResult, allListOfSpecies, allImages, screenWidth);
          } else {
           result.showResultOnPhone(indexSpeciesforResult, allListOfSpecies, allImages, screenWidth*2);
          }
        }
        // var that = this;
        window.addEventListener("orientationchange", function() {
          // that.debug(window.screen.availWidth);
          if (screen.orientation.type === "landscape-primary" || screen.orientation.type === "landscape-secondary" || screen.orientation.type === "landscape") {
            // that.debug('landscape');
            var landscapeScreenWidth = window.screen.availWidth-(window.screen.availWidth*0.1);
            result.showResults(indexSpeciesforResult, allListOfSpecies, allImages, landscapeScreenWidth);
          } else {
            var portraitScreenWidth = window.screen.availWidth;
            if (result._isTablet == true) {
              result.showResults(indexSpeciesforResult, allListOfSpecies, allImages, portraitScreenWidth);
            } else {
              result.showResultOnPhone(indexSpeciesforResult, allListOfSpecies, allImages, portraitScreenWidth*2);
            }
          }
        });
      }, this);

      // Show more informations about specie in additional to his images
      result.addListener("tapdOnSpecie", function(evt) {
        // this.debug("you type on specie : " + evt.getData());
        // var des = new qx.ui.mobile.form.Label(evt.getData() + " ?");
        // description.removeAll(des);
        // description.add(des);
        // description.show();
        console.log(evt.getData());
        description.displaySpecieDes("resource/ifungi/Markdown/"+evt.getData()+".md");
        description.show();
      }, this);

      // If there is no checkBox Checked, tell user to select criteria
      bd.addListener("noCheckboxChecked", function(evt) {
        result.hide();
        this.debug(evt.getData());
        var label = new qx.ui.mobile.form.Label(this.tr("Please select criteria to see results !!"));
        voidpage.removeAll();
        voidpage.add(label);
        voidpage.show();
      }, this);

      // Display more information about a criterion
      critere.addListener("displayCritereHelp", function(evt) {
        // console.log(evt.getData());
        let lab = new qx.ui.mobile.form.Label(evt.getData());
        help.removeAll(lab);
        help.add(lab);
        help.show();
      }, this);

      // Return to the Menu
      critere.addListener("back", function(evt) {
        menu.show({
          reverse: true
        });
      }, this);
      // Return to the Menu
      overview.addListener("back", function(evt) {
        menu.show({
          reverse: true
        });
      }, this);
      // Return to the Critere Page
      result.addListener("back", function(evt) {
        critere.show({
          reverse: true
        });
      }, this);
      // Return to the Critere Page
      voidpage.addListener("back", function(evt) {
        result.hide();
        critere.show({
          reverse: true
        });
      }, this);
      // Return to the Result Page
      description.addListener("back", function(evt) {
        result.show({
          reverse: true
        });
      }, this);
      // Return to the Overview page
      help.addListener("back", function(evt) {
        voidpage.show({
          reverse: true
        });
      }, this);


      // // Back to top Button for Result Page
      // var top = new qx.ui.mobile.container.Composite();
      // top.add(new qx.ui.mobile.basic.Image("resource/ifungi/BacktoTop.png"));
      // top.addCssClass("top");
      // // top.setDefaultCssClass("top");
      // // result.add(top);
      // voidpage.add(top);
      // // result.getRightContainer().add(top);
      // // top.setVisibility("excluded");
      // top.addListener("tap", function(evt) {
      //   this.debug("aa");
      //   // result.__scrollContainer.__containerElement.scrollTop = 0;
      //   // // result.__scrollContainer.__contentElement.scrollTop = 0;
      //   // top.setVisibility("excluded");
      // }, this);
      // // result.addListener("roll", function(evt) {
      // //   // this.debug("aa");
      // //   // this.debug(result.__scrollContainer.__containerElement.scrollTop);
      // //   // if (result.__scrollContainer.__contentElement.scrollTop > 20) {
      // //   if (result.__scrollContainer.__containerElement.scrollTop > 20) {
      // //     top.setVisibility("visible");
      // //   }else {
      // //     top.setVisibility("excluded");
      // //   }
      // // }, this);

      // Back to top Button for Result Page
      var top = new qx.ui.mobile.container.Composite();
      top.add(new qx.ui.mobile.basic.Image("resource/ifungi/BacktoTop.png"));
      result.getRightContainer().add(top);
      top.setVisibility("excluded");
      // top.addListener("tap", function(evt) {
      //   result.__scrollContainer.__containerElement.scrollTop = 0;
      //   // result.__scrollContainer.__contentElement.scrollTop = 0;
      //   top.setVisibility("excluded");
      // }, this);
      // // result.add(top)
      // result.addListener("roll", function(evt) {
      //   // if (result.__scrollContainer.__contentElement.scrollTop > 20) {
      //   if (result.__scrollContainer.__containerElement.scrollTop > 20) {
      //     top.setVisibility("visible");
      //   } else {
      //     top.setVisibility("excluded");
      //   }
      // }, this);

      // // var top = new qx.ui.mobile.form.Button("Top");
      // var top = new qx.ui.mobile.container.Composite();
      // top.add(new qx.ui.mobile.basic.Image("resource/ifungi/BacktoTop.png"));
      // top.setVisibility("excluded");
      // this.debug(overview);
      // top.addListener("tap", function(evt) {
      //   overview.__scrollContainer.__containerElement.scrollTop = 0;
      //   top.setVisibility("excluded");
      // }, this);
      // overview.getRightContainer().add(top);
      // overview.addListener("roll", function(evt) {
      //   if (overview.__scrollContainer.__containerElement.scrollTop > 20) {
      //     top.setVisibility("visible");
      //   }else {
      //     top.setVisibility("excluded");
      //   }
      // }, this);

      // Application langage version (french or english)
      var lang = new qx.ui.mobile.basic.Image("resource/ifungi/fr.png");
      // console.log(lang.getSource());
      var composite = new qx.ui.mobile.container.Composite();
      composite.add(lang);
      var manag = qx.locale.Manager.getInstance();
      // menu.getLeftContainer().add(composite);
      // menu.setButtonIcon("resource/ifungi/fr.png");
      // menu.setShowButton(true);
      menu.getLeftContainer().add(composite);
      // console.log(manag.getLocale());
      composite.addListener("tap", function(evt) {
        // manag.addListener("changeLocale", function(evt) {
        //   console.log("bb");
        // }, this);
        // console.log('aie');
        if (manag.getLocale() == "en_ie") {
          // console.log('aa');
          // lang.setSource("resource/ifungi/eng.png");
          manag.setLocale("fr");
          // menu.setButtonIcon("resource/ifungi/eng.png");
          lang.setSource("resource/ifungi/eng.png");
          arbre.getJsonfile("resource/ifungi/arbres/micromycetes/fileFR.json");
          bd.getfilecsv("resource/ifungi/datasets/micromycetes/fileFR.csv");
          // arbre.addListener("loadedJsonFile", function(evt) {
          //   critere.showTree(evt.getData());
          // }, this);
          // critere.show();
        }else {
          // console.log("bb");
          manag.setLocale("en_ie");
          // menu.setButtonIcon("resource/ifungi/fr.png");
          lang.setSource("resource/ifungi/fr.png");
          critere.removeAll();
          arbre.getJsonfile("resource/ifungi/arbres/micromycetes/fileEN.json");
          bd.getfilecsv("resource/ifungi/datasets/micromycetes/fileEN.csv");
          // arbre.addListener("loadedJsonFile", function(evt) {
          //   critere.showTree(evt.getData());
          // }, this);
        }
      }, this);

      // var manag = qx.locale.Manager.getInstance();
      // // // menu.add(composite);
      // // menu.getLeftContainer().add(composite);
      // menu.setButtonIcon("resource/ifungi/fr.png");
      // menu.setShowButton(true);
      // // console.log(manag.getLocale());
      // menu.addListener("action", function(evt) {
      //   // manag.addListener("changeLocale", function(evt) {
      //   //   console.log("bb");
      //   // }, this);
      //   // console.log('aie');
      //   if (manag.getLocale() == "en_ie") {
      //     // console.log('aa');
      //     manag.setLocale("fr");
      //     menu.setButtonIcon("resource/ifungi/eng.png");
      //   }else {
      //     manag.setLocale("en_ie");
      //     menu.setButtonIcon("resource/ifungi/fr.png");
      //   }
      // }, this);

      this.getRouting().init();

      arbre.getJsonfile("resource/ifungi/arbres/micromycetes/fileEN.json");
      bd.getfilecsv("resource/ifungi/datasets/micromycetes/fileEN.csv");

      if (navigator !== undefined && navigator.splashscreen !== undefined) {
        navigator.splashscreen.hide();
      }
    }
  }
});
