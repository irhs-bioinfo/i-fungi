/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * TODO: needs documentation
 */
qx.Class.define("ifungi.data.BD",
{
  type: "singleton",
  extend : qx.core.Object,

  properties :
  {
    /** For all indexes of criteres corresponding to species in database */
    indexesOfAllCriteres :
    {
      check : "Object",
      nullable : true,
      event : "changeIndexesOfAllCriteres"
    },
    /** For all species in database */
    speciesList :
    {
      check : "Object",
      nullable : true,
      event : "changeSpeciesList"
    },
    /** For indexes of Species Images in database */
    allImages :
    {
      check : "Array",
      nullable : true,
      event : "changeAllImages"
    },
    /**
    * Array containing all criteres in datacsv
    */
    allCriteresInDataBase :
    {
      check : "Object",
      nullable : true,
      event : "changeAllCriteresInDataBase"
    }
  },

  events : {
    /**
    * Fired for geting the indexes of species in database to load
    */
    "getSpeciesIndexesFinal" : "qx.event.type.Data",
    /**
    * Fired for geting the count of checkBox checked
    */
    "noCheckboxChecked" : "qx.event.type.Data"
  },

  construct : function(value) {
    this.base(arguments);
  },

  members :
  {
    // overridden
    _initialize : function() {
      this.base(arguments);
    },
    /**
     * Parse the csv file (database)
     * @param file {Object} Object containing the criteres.
     * returns a large array in which each CSV column is storage as
     * a array with for first element, the header of the column.
    */
    getfilecsv : function(file) {
      var req = new qx.io.request.Xhr(file);
      // this.debug(req);
      // this.debug(req.__phase);
      // For if file is loaded completly with success
      req.addListener("success", function(e) {
        var req = e.getTarget();
        // this.debug(req.__phase);
        // Response parsed according to the server's
        // response content type, e.g. JSON
        var response = req.getResponse();
        // this.debug(response);
        var lines = response.split(/\n/);
        // this.debug(lines);
        var tabglobalOfCriteres = [];
        var headlines = lines[0].split(/,/);
        // this.debug(headlines.length);
        for (let i = 0; i < headlines.length; i++) {
          var tab = [];
          tabglobalOfCriteres.push(tab);
        }
        for (let line = 0; line < lines.length; line++) {
          var linestab = lines[line].split(/,/);
          // this.debug(linestab);
          for (let i = 0; i < tabglobalOfCriteres.length; i++) {
            tabglobalOfCriteres[i].push(linestab[i]);
          }
        }
        // this.debug(tabglobalOfCriteres);
        this.setAllCriteresInDataBase(tabglobalOfCriteres);
        // this.fireDataEvent("allCriteres", tabglobalOfCriteres);
        this.debug("CSV data successfully loaded");
      }, this);
      //  For if file don't load completly with success
      req.addListener("fail", function(e) {
        this.debug("CSV data loaded with failure");
      }, this);
      // Send request
      req.send();
    },
    /**
      * Takes the array of criteria returns by the function getfilecsv()
      * returns array of species and indexes of species images to display
    */
    getAllIndexesOfAllCriteres : function() {
      var tabglobalOfCriteres = this.getAllCriteresInDataBase();
      // var myObjectGlobalOfCritre = {};
      var indexCriteres = [];
      var allImages = [];
      for (let j = 0; j < tabglobalOfCriteres.length; j++) {
        // this.debug(tabglobalOfCriteres[j]);
        if (tabglobalOfCriteres[j][0] == "Photo genre" || tabglobalOfCriteres[j][0] == "Photo micro genre" || tabglobalOfCriteres[j][0] == "Dessus" || tabglobalOfCriteres[j][0] == "Dessous" || tabglobalOfCriteres[j][0] == "Photo micro") {
          // this.debug(tabglobalOfCriteres[j]);
          allImages.push(tabglobalOfCriteres[j]);
        } else {
          var myObject = {};
          var critere = new ifungi.page.Critere();
          var array = tabglobalOfCriteres[j].map(function (val) {
           return critere.cleanUpSpecialChars(val);
          });
          // this.debug(array);
          if (array[0] == "espece") { // Species tab
            this.debug(array[0]);
            this.setSpeciesList(array);
          }
          for (let i = 0; i < array.length; i++) {
            var stringToFind = array[i];
            var indexesOfcritere = [];
            array.forEach(function(elem, index, array) {
                if (elem === stringToFind) {
                  indexesOfcritere.push(index);
                }
                return indexesOfcritere;
            });
            // this.debug(stringToFind);
            // this.debug(indexesOfcritere);
            myObject[stringToFind] = indexesOfcritere;
          }
          // this.debug(myObject);
          indexCriteres.push(myObject);
        }
      }
      // this.debug("indexes of Criteres");
      // this.debug(indexCriteres);
      this.setIndexesOfAllCriteres(indexCriteres);
      // this.debug(allImages);
      this.setAllImages(allImages);
    },
    /**
     * return le final indexes of criteria choseen by user
     * after doing intersection beetween indexes of criteria
     * @param dicoCriteres {Array} Array list of criteria choseen by user.
    */
    rechercheEspeces : function (dicoCriteres) {
      this.debug("--------------------------------------------------------------");
      var tabResultCriteres = [];
      var nbCheckboxChecked = 0;
      // var speciestab = this.getSpeciesList();
      // this.debug(speciestab);
      if (this.getSpeciesList() != null) {
        var indexCriterestab = this.getIndexesOfAllCriteres();
        // this.debug(indexCriterestab);
        // this.debug(dicoCriteres);
        for (let i = 0; i < dicoCriteres.length; i++) {
          // this.debug(dicoCriteres[i]);
          var critereSelect = dicoCriteres[i];
          this.debug(critereSelect);

          // var keys = Object.keys(critereSelect);
          Object.entries(critereSelect).forEach(([key, value]) => {
             var pereCritereSelect = key;
             var valueCritereSelect = value;
             for (let j = 0; j < indexCriterestab.length; j++) {
               var obj = indexCriterestab[j];
               // this.debug(obj);
               // this.debug(pereCritereSelect);
               // this.debug(Object.keys(obj)[0]);
               if (pereCritereSelect == Object.keys(obj)[0]) {
                 nbCheckboxChecked++;
                 if (valueCritereSelect in obj) {
                   tabResultCriteres.push(obj[valueCritereSelect]);
                 } else {
                   this.debug("no indexes for " + valueCritereSelect);
                   tabResultCriteres.push([]);
                 }
               }
             }
          });
        }
        var finalSpeciesIndexes = [];
        if (tabResultCriteres.length != 0) {
          finalSpeciesIndexes = tabResultCriteres.reduce((a, b) => a.filter(c => b.includes(c)));
        }
        this.fireDataEvent("getSpeciesIndexesFinal", finalSpeciesIndexes);
        // If there is no checkbox checked
        if (nbCheckboxChecked == 0) {
          this.debug(nbCheckboxChecked);
          this.fireDataEvent("noCheckboxChecked", nbCheckboxChecked);
        }
      }
    }
  }
});
