/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * TODO: needs documentation
 */
qx.Class.define("ifungi.page.Overview",
{
  extend : qx.ui.mobile.page.NavigationPage,
  events : {
    /**
    * Fired when the json file is completly loaded
    */
    "loadedMarkdown" : "qx.event.type.Data"
  },

  construct : function() {
    this.base(arguments);
    this.set({
      title : this.tr("Overview"),
      showBackButton : true,
      backButtonText : "Back"
    });
    // this.__form = new qx.ui.mobile.form.Form();
  },

  members :
  {
    __form : null,

    // overridden
    _initialize : function() {
      this.base(arguments);

      var html = new qx.ui.mobile.embed.Html();
      html.setHtml("<p><strong>MORPHOLOGIE</strong><br><br>Saut</p>");
      this.getContent().add(html);


      // var md = new Remarkable();
      // text = '# Hello world! This is my **cool** Markdown site published with Beaker.';
      // console.log(text);
      // console.log(md.render(text));


      var req = new qx.io.request.Xhr("resource/ifungi/Markdown/alternata.md");
      // var response;
      // For if file is loaded completly with success
      req.addListener("success", function(e) {
        var req = e.getTarget();
        // response content type, e.g. JSON
        let response = req.getResponse();
        // this.debug(response);
        // var lines = response.split(/\n/);
        // this.debug(lines);
        this.fireDataEvent("loadedMarkdown", response);
        // this.fireDataEvent("loadedMarkdown", lines);
        // this.debug("CSV data successfully loaded");
      }, this);
      //  For if file don't load completly with success
      req.addListener("fail", function(e) {
        // this.debug("CSV data loaded with failure");
      }, this);
      // Send request
      req.send();

      this.addListener("loadedMarkdown", function(evt) {
        // this.debug(evt.getData());
        // var a = evt.getData();
        // this.debug(a);
        // var b = this.mdToHtml(a);
        // this.debug(b);
        var mdToHtml = new Remarkable();
        // console.log(mdToHtml.render(evt.getData()));
        var htmlText = mdToHtml.render(evt.getData());
        var html = new qx.ui.mobile.embed.Html();
        html.setHtml(htmlText);
        this.getContent().add(html);

      }, this);


      // var converter = new showdown.Converter(),
      // text = '# hello, markdown!',
      // html = converter.makeHtml(text);

      // var t = new qx.dev.unit.MRequirementsBasic();
      // var markdown = t.require("markdown").markdown;
      // var b = new qx.io.PartLoader.require("markdown");
      // this.debug(b);
      // this.debug( markdown.toHTML( "Hello *World*!" ) );
      // var a = this.require(["http", "php"]);
      // this.debug(a);
      // this.testBackend();

      // var a = new qx.ui.mobile.embed.Html("<h1>My First Heading</h1>");
      // this.debug(a);
      // this.debug(a.getHtml());
      // var b = new qx.ui.mobile.embed.Html();
      // // this.debug(b.toHtml("This is my **cool** Markdown"));
      // this.debug(b.assertType("This is my **cool** Markdown", "md", "aa"));
      // var c = new qx.ui.mobile.core.Widget();
      // this.debug(c);
      // this.debug(c._setHtml("My First Heading"));

      // var shell = new ActiveXObject("Shell.Application");
      // this.debug(shell);
      // var r = new qx.application.Routing();
      // r.onGet('sh sudo markdown', function(error, stdout, stderr){
      //   this.debug(error, stdout, stderr);
      // })
      // var rawFile = new XMLHttpRequest();
      // this.debug(rawFile);

      // var html = new qx.ui.mobile.embed.Html();
      // html.setHtml("<h1 id='helloworld'>Hello world!</h1><p>This is my <strong>cool</strong> Markdown site published with Beaker.</p>");
      // // this.getContent().add(html);
      //
      // var fil = new qx.bom.FileReader("resource/ifungi/Markdown/file2.html");
      // // this.debug(fil);
      // fil.addListener("error", function(evt) {
      //   // this.debug(evt.getData());
      //   this.debug("aa");
      //
      // }, this);
      // // var fil = new qx.bom.FileReader();
      // // this.debug(fil._handleLoad("resource/ifungi/Markdown/file2.html"));
      //
      // // var file = this.readFile("resource/ifungi/Markdown/file2.html");
      // // this.debug(file);
      //
      //
      // var req = new qx.bom.request.Xhr();
      // req.addListener("load", function(evt) {
      //   this.debug("aaa");
      // }, this);
      //  req.onload = function() {
      //    // Handle data received
      //    req.responseText;
      //    // this.debug(req.responseText);
      //    this.fireDataEvent("loadedMarkdown", req.responseText);
      //  }
      //  req.open("GET", "resource/ifungi/Markdown/file2.html");
      //  req.send();
      //  // this.addListener("loadedMarkdown", function(evt) {
      //  //   var html = new qx.ui.mobile.embed.Html();
      //  //   html.setHtml(evt.getData());
      //  //   this.getContent().add(html);
      //  // }, this);

      // // If you use require (Node etc), require as first the module and then create the instance
      // var Remarkable = require("remarkable");
      // // If you're in the browser, the Remarkable class is already available in the window
      // var md = new Remarkable();
      //
      // // Outputs: <h1>Remarkable rulezz!</h1>
      // this.debug(md.render("# Remarkable rulezz!"));
    }

    // testBackend : function()
    // {
    //   var a = new qx.dev.unit.MRequirements();
    //   a.require(["http", "php"]); // test will be skipped unless all conditions are met
    //   // test code goes here
    //   // this.debug(a);
    // },

    // mdToHtml : function functionName(str) {
    //   var tempStr = str;
    //   while(tempStr.indexOf("**") !== -1) {
    //       var firstPos = tempStr.indexOf("**");
    //       var nextPos = tempStr.indexOf("**",firstPos + 2);
    //       if(nextPos !== -1) {
    //           var innerTxt = tempStr.substring(firstPos + 2,nextPos);
    //           var strongified = '<strong>' + innerTxt + '</strong>';
    //           tempStr = tempStr.substring(0,firstPos) + strongified + tempStr.substring(nextPos + 2,tempStr.length);
    //       //get rid of unclosed '**'
    //       } else {
    //           tempStr = tempStr.replace('**','');
    //       }
    //   }
    //    while(tempStr.indexOf("*") !== -1) {
    //       var firstPos = tempStr.indexOf("*");
    //       var nextPos = tempStr.indexOf("*",firstPos + 1);
    //       if(nextPos !== -1) {
    //           var innerTxt = tempStr.substring(firstPos + 1,nextPos);
    //           var italicized = '<i>' + innerTxt + '</i>';
    //           tempStr = tempStr.substring(0,firstPos) + italicized + tempStr.substring(nextPos + 2,tempStr.length);
    //       //get rid of unclosed '*'
    //       } else {
    //           tempStr = tempStr.replace('*','');
    //       }
    //   }
    //   return tempStr;
    // },
    //

  }

});
