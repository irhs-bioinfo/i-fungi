/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * TODO: needs documentation
 */
qx.Class.define("ifungi.page.DescriptionSpecie",
{
  extend : qx.ui.mobile.page.NavigationPage,
  events : {
    /**
    * Fired when the Markdown file is completly loaded
    */
    "loadedMarkdown" : "qx.event.type.Data"
  },

  construct : function() {
    this.base(arguments);
    this.set({
      title : this.tr("Description of Specie"),
      // title : this.tr("Description Espece"),
      showBackButton : true,
      backButtonText : "Back"
    });
  },


  members :
  {
    // overridden
    _initialize : function() {
      this.base(arguments);

      // var md = new Remarkable();
      // this.debug(md);
      // this.debug(md.render('# Remarkable rulezz!'));
    },
    displaySpecieDes : function(specieMD) {
      var req = new qx.io.request.Xhr(specieMD);
      // For if file is loaded completly with success
      req.addListener("success", function(e) {
        var req = e.getTarget();
        let response = req.getResponse();
        this.fireDataEvent("loadedMarkdown", response);
        this.debug("MD file successfully loaded");
      }, this);
      //  For if file don't load completly with success
      req.addListener("fail", function(e) {
        this.debug("MD file loaded with failure");
        var label = new qx.ui.mobile.form.Label(this.tr("Sorry my description is not ready yet"));
        this.getContent().add(label);
      }, this);
      // Send request
      req.send();

      var mdToHtml = new Remarkable();
      var html = new qx.ui.mobile.embed.Html();
      this.addListener("loadedMarkdown", function(evt) {
        console.log(evt.getData());
        let oldHtml = mdToHtml.render(evt.getData());
        console.log(oldHtml);
        let newHtml0 = oldHtml.replace(/<img/g, '<img height="300"');
        let newHtml = newHtml0.replace(/SAUTDELINE/g, '<br>');
        console.log(newHtml);
        // html.setHtml(oldHtml);
        html.setHtml(newHtml);

      }, this);
      this.getContent().removeAll(html);
      this.getContent().add(html);
    }

  }
});
