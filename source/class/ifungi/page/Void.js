/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * TODO: needs documentation
 */
qx.Class.define("ifungi.page.Void",
{
  extend : qx.ui.mobile.page.NavigationPage,

  construct : function() {
    this.base(arguments);
    this.set({
      // title : "voidPage",
      showBackButton : true,
      backButtonText : "Back"
    });
  },


  members :
  {
    // overridden
    _initialize : function() {
      this.base(arguments);
      // this.setBackground("#000616");
      this.setLayout(new qx.ui.mobile.layout.HBox());
      this.addCssClass("void");
      // this.getContent().add(new qx.ui.mobile.basic.Image("resource/ifungi/blue_mycelium_small.jpg"), {flex: 1});
    }
    // // overridden
    // _back : function(triggeredByKeyEvent)
    // {
    //   qx.core.Init.getApplication().getRouting().back();
    // }
  }
});
