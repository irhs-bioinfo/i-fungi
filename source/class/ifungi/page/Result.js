/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * TODO: needs documentation
 */
qx.Class.define("ifungi.page.Result",
{
  extend : qx.ui.mobile.page.NavigationPage,

  events : {
    /**
     * Fired when specie item is typed
     * for show specie description page
     */
    "tapdOnSpecie" : "qx.event.type.Data"
  },

  construct : function() {
    this.base(arguments);
    this.set({
      title : this.tr("Result"),
      showBackButton : true,
      backButtonText : "Back"
    });
  },

  members :
  {
    // overridden
    _initialize : function() {
      this.base(arguments);

      // var top = new qx.ui.mobile.container.Composite();
      // top.add(new qx.ui.mobile.basic.Image("resource/ifungi/BacktoTop.png"));
      // // top.addCssClass("top");
      // // top.setDefaultCssClass("top");
      // // result.add(top);
      //
      // // result.getRightContainer().add(top);
      // // top.setVisibility("excluded");
      // top.addListener("tap", function(evt) {
      //   this.debug("aa");
      //   // result.__scrollContainer.__containerElement.scrollTop = 0;
      //   // // result.__scrollContainer.__contentElement.scrollTop = 0;
      //   // top.setVisibility("excluded");
      // }, this);
      //
      // this.getContent().add(top);
    },
    /**
     * Show user request result on the screen corresponding to species images
     * @param indexSpeciesforResult {Array} Array containing indexes of species of criteria chooseen by user
     * @param allListOfSpecies {Array} Array containing list of all species
     * @param allImages {Array} Array containing tables of images
     */
    showResults : function (indexSpeciesforResult, allListOfSpecies, allImages, screenWidth) {
      // this.debug(indexSpeciesforResult);
      // this.debug(allListOfSpecies);
      // this.debug(allImages);
      this.getContent().removeAll();
      var resultVBox = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignY:"middle"}));
      var hitsHBox = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox().set({alignY:"middle"}));
      var carouselContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignY:"middle"}));

      if (indexSpeciesforResult.length != 0) {
        /**
         * Number of hits found
        */
        var hitsNumber = new qx.ui.mobile.form.Label(indexSpeciesforResult.length+ this.tr(" HITS"));
        var sort = new qx.ui.mobile.form.Label(this.tr("sort by GENUS or <br>SPECIES"));
        if (hitsNumber != null) {
          hitsHBox.add(hitsNumber, {flex:1});
          hitsHBox.add(sort);
        }
        var isTabletCarousel = [];
        var isTabletNameSpecie = [];
        this.debug("Species to load");
        for (let i = 0; i < indexSpeciesforResult.length; i++) {
          var itemSpecie = allListOfSpecies[indexSpeciesforResult[i]];
          this.debug(indexSpeciesforResult[i] + "  " + allListOfSpecies[indexSpeciesforResult[i]]);
          // Elimination of duplicates in the images list to show
          var uniqueImages = [];
          for (let l = 0; l < allImages.length; l++) {
            var index = uniqueImages.findIndex(x => x==allImages[l][indexSpeciesforResult[i]]);
            if (index === -1) {
              if (allImages[l][indexSpeciesforResult[i]] != "") { // if not enmpty value
                uniqueImages.push(allImages[l][indexSpeciesforResult[i]]);
              }
            }
          }
          this.debug(uniqueImages);
          /**
            * Addition species and images of specice in a component
            * depending on whether it is a tablet or a smarphones
          */
          var carousel = new qx.ui.mobile.container.Carousel();
          var nameSpecieContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignX:"center"}));
          var hboxNameSpecie = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox().set({alignX:"center"}));
          var nameSpecie = new qx.ui.mobile.form.Label(allListOfSpecies[indexSpeciesforResult[i]]);
          hboxNameSpecie.add(nameSpecie);
          hboxNameSpecie.add(new qx.ui.mobile.basic.Image("resource/ifungi/btn_scopic2.png"));
          nameSpecieContainer.add(hboxNameSpecie);
          // addListener on name of specie for shown the description of specie
          hboxNameSpecie.addListener("tap", this.bindClick(itemSpecie), this);
          // function bindClick(itemSpecie) {
          //   return function() {
          //       this.debug("you type on specie : " + itemSpecie);
          //       this.fireDataEvent("tapdOnSpecie", itemSpecie);
          //   };
          // }
          // Display Images
          if (uniqueImages.length != 0) {
            for (let m = 0; m < uniqueImages.length; m++) {
              this.debug(uniqueImages[m]);
              let source = "resource/ifungi/FolderImages/"+uniqueImages[m]+".jpg";
              var imageSpecie = new qx.ui.mobile.basic.Image(source);
              // Resise Images
              this.resizeImage(imageSpecie, carousel, screenWidth);
              // addListener on image of specie for shown the description of specie
              imageSpecie.addListener("tap", this.bindClick(itemSpecie), this);
              // function bindClick(itemSpecie) {
              //   return function() {
              //       this.debug("you type on specie : " + itemSpecie);
              //       this.fireDataEvent("tapdOnSpecie", itemSpecie);
              //   };
              // }
              let imageSpecieContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignX:"center"}));
              imageSpecieContainer.add(imageSpecie);
              carousel.add(imageSpecieContainer);
            }
          } else {
            let source = "resource/ifungi/FolderImages/defaultImage.jpg";
            var defaultImageSpecie = new qx.ui.mobile.basic.Image(source);
            // Resise Images
            this.resizeImage(defaultImageSpecie, carousel, screenWidth);
            // addListener on image of specie for shown the description of specie
            defaultImageSpecie.addListener("tap", this.bindClick(itemSpecie), this);
            // function bindClick(itemSpecie) {
            //   return function() {
            //       this.debug("you type on specie : " + itemSpecie);
            //       this.fireDataEvent("tapdOnSpecie", itemSpecie);
            //   };
            // }
            let imageSpecieContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignX:"center"}));
            imageSpecieContainer.add(defaultImageSpecie);
            carousel.add(imageSpecieContainer);
          }

          isTabletNameSpecie.push(nameSpecieContainer);
          isTabletCarousel.push(carousel);
        }
        var isTabletNameSplit = this.__splitPairs(isTabletNameSpecie);
        var isTabletCarouselSplit = this.__splitPairs(isTabletCarousel);
        for (let i = 0; i < isTabletCarouselSplit.length; i++) {
          var hboxForNames = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox().set({alignY:"middle"}));
          var hboxForCarousel = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox().set({alignY:"middle"}));
          this.debug(isTabletCarouselSplit[i]);
          for (let j = 0; j < isTabletCarouselSplit[i].length; j++) {
            hboxForNames.add(isTabletNameSplit[i][j]);
            hboxForCarousel.add(isTabletCarouselSplit[i][j]);
          }
          carouselContainer.add(hboxForNames);
          carouselContainer.add(hboxForCarousel);
        }
      } else {
        carouselContainer.add(new qx.ui.mobile.form.Label(this.tr("There is no Species")));
        this.debug("no Species to load");
      }
      resultVBox.add(hitsHBox);
      resultVBox.add(carouselContainer);
      this.getContent().add(resultVBox);
    },

    showResultOnPhone : function (indexSpeciesforResult, allListOfSpecies, allImages, screenWidth) {
      // this.debug(indexSpeciesforResult);
      // this.debug(allListOfSpecies);
      // this.debug(allImages);
      this.getContent().removeAll();
      var resultVBox = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignY:"middle"}));
      var hitsHBox = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox().set({alignY:"middle"}));
      var carouselContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignY:"middle"}));

      if (indexSpeciesforResult.length != 0) {
        /**
         * Number of hits found
        */
        var hitsNumber = new qx.ui.mobile.form.Label(indexSpeciesforResult.length+ this.tr(" HITS"));
        var sort = new qx.ui.mobile.form.Label(this.tr("sort by GENUS or <br>SPECIES"));
        if (hitsNumber != null) {
          hitsHBox.add(hitsNumber, {flex:1});
          hitsHBox.add(sort);
        }

        // var isTabletCarousel = [];
        // var isTabletNameSpecie = [];
        this.debug("Species to load");
        for (let i = 0; i < indexSpeciesforResult.length; i++) {
          var itemSpecie = allListOfSpecies[indexSpeciesforResult[i]];
          this.debug(indexSpeciesforResult[i] + "  " + allListOfSpecies[indexSpeciesforResult[i]]);
          // Elimination of duplicates in the images list to show
          var uniqueImages = [];
          for (let l = 0; l < allImages.length; l++) {
            var index = uniqueImages.findIndex(x => x==allImages[l][indexSpeciesforResult[i]]);
            if (index === -1) {
              if (allImages[l][indexSpeciesforResult[i]] != "") { // if not enmpty value
                uniqueImages.push(allImages[l][indexSpeciesforResult[i]]);
              }
            }
          }
          this.debug(uniqueImages);
          /**
            * Addition species and images of specice in a component
            * depending on whether it is a tablet or a smarphones
          */
          var carousel = new qx.ui.mobile.container.Carousel();
          var nameSpecieContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignX:"center"}));
          var hboxNameSpecie = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox().set({alignX:"center"}));
          var nameSpecie = new qx.ui.mobile.form.Label(allListOfSpecies[indexSpeciesforResult[i]]);
          hboxNameSpecie.add(nameSpecie);
          hboxNameSpecie.add(new qx.ui.mobile.basic.Image("resource/ifungi/btn_scopic2.png"));
          nameSpecieContainer.add(hboxNameSpecie);
          // addListener on name of specie for shown the description of specie
          hboxNameSpecie.addListener("tap", this.bindClick(itemSpecie), this);
          // function bindClick(itemSpecie) {
          //   return function() {
          //       this.debug("you type on specie : " + itemSpecie);
          //       this.fireDataEvent("tapdOnSpecie", itemSpecie);
          //   };
          // }
          // Display Images
          if (uniqueImages.length != 0) {
            for (let m = 0; m < uniqueImages.length; m++) {
              this.debug(uniqueImages[m]);
              let source = "resource/ifungi/FolderImages/"+uniqueImages[m]+".jpg";
              var imageSpecie = new qx.ui.mobile.basic.Image(source);

              // Resise Images
              this.resizeImage(imageSpecie, carousel, screenWidth);

              // addListener on image of specie for shown the description of specie
              imageSpecie.addListener("tap", this.bindClick(itemSpecie), this);
              // function bindClick(itemSpecie) {
              //   return function() {
              //       this.debug("you type on specie : " + itemSpecie);
              //       this.fireDataEvent("tapdOnSpecie", itemSpecie);
              //   };
              // }

              let imageSpecieContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignX:"center"}));
              imageSpecieContainer.add(imageSpecie);
              carousel.add(imageSpecieContainer);
            }
          } else {
            let source = "resource/ifungi/FolderImages/defaultImage.jpg";
            var defaultImageSpecie = new qx.ui.mobile.basic.Image(source);

            // Resise Images
            this.resizeImage(defaultImageSpecie, carousel, screenWidth);

            // addListener on image of specie for shown the description of specie
            defaultImageSpecie.addListener("tap", this.bindClick(itemSpecie), this);
            // function bindClick(itemSpecie) {
            //   return function() {
            //       this.debug("you type on specie : " + itemSpecie);
            //       this.fireDataEvent("tapdOnSpecie", itemSpecie);
            //   };
            // }
            let imageSpecieContainer = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox().set({alignX:"center"}));
            imageSpecieContainer.add(defaultImageSpecie);
            carousel.add(imageSpecieContainer);
          }

          carouselContainer.add(nameSpecieContainer);
          carouselContainer.add(carousel);
        }
      } else {
        carouselContainer.add(new qx.ui.mobile.form.Label(this.tr("There is no Species")));
        this.debug("no Species to load");
      }
      resultVBox.add(hitsHBox);
      resultVBox.add(carouselContainer);
      this.getContent().add(resultVBox);
    },

    bindClick : function(itemSpecie) {
      return function() {
          this.debug("you type on specie : " + itemSpecie);
          this.fireDataEvent("tapdOnSpecie", itemSpecie);
      };
    },

    /**
      * Split a large Array in the several arrays of two element
      * @param array {Array} Array whose split for display result
    */
    __splitPairs : function(arr) {
      var pairs = [];
      for (let i=0; i<arr.length; i+=2) {
          if (arr[i+1] !== undefined) {
              pairs.push([arr[i], arr[i+1]]);
          } else {
              pairs.push([arr[i]]);
          }
      }
      return pairs;
    },
    /**
      * Resize Images to display
      * That define the same dimension (height and width) to all images.
      * @param imag {Image} Image to resize
    */
    resizeImage : function(imag, carousel, clientWidth) {
      if (imag.__containerElement !== undefined) {
        imag.__containerElement.width = (clientWidth / 2) - (clientWidth * 0.1);
        imag.__containerElement.height = (clientWidth / 2) - (clientWidth * 0.1);
        // if (this._isTablet == true) {
        //   imag.__containerElement.width = (clientWidth/2)-(clientWidth*0.1);
        //   imag.__containerElement.height = (clientWidth/2)-(clientWidth*0.1);
        // }else {
        //   imag.__containerElement.width = (clientWidth)-(clientWidth*0.1);
        //   imag.__containerElement.height = (clientWidth)-(clientWidth*0.1);
        // }
        carousel.setHeight(imag.__containerElement.height);
      }
    }
  }
});
