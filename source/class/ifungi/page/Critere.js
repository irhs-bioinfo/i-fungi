/* ************************************************************************

   Copyright: 2019 INRA

   License: CeCILL

   Authors: Alfred Goumou (fredgoum) alfredgoumou@gmail.com

************************************************************************ */

/**
 * TODO: needs documentation
 */
qx.Class.define("ifungi.page.Critere",
{
  extend : qx.ui.mobile.page.NavigationPage,
  // extend : qx.core.Object,

  events : {
    /**
     * Fired when the checkBox are checked
     */
    "ajouterCritere" : "qx.event.type.Data",
    /**
     * Fired when the checkBox are unchecked
     */
    "enleverCritere" : "qx.event.type.Data",
    /**
     * Fired for unchecked all checkBoxes
     */
    // "uncheckedChecboxes" : "qx.event.type.Data"
    /**
     * Display page for more information about a critere
     */
     "displayCritereHelp" : "qx.event.type.Data"
  },

  construct : function() {
    this.base(arguments);
    this.set({
      title : this.tr("Criteria"),
      // title : this.tr("Criteres"),
      showBackButton : true,
      backButtonText : "Back"
    });
    // this.__form = new qx.ui.mobile.form.Form();
    // this.__checkBoxform = new qx.ui.mobile.form.renderer.Single();
  },

  members :
  {
    // __form : null,
    // __checkBoxform : null,

    // overridden
    _initialize : function() {
      this.base(arguments);
      // var arbre = ifungi.data.Arbre.getInstance();
      // var myNewJSON = arbre.getJsonfile();
      // // this.debug(myNewJSON);
      // Bouton pour decocher tous les checkBoxs
      // var bouton = new qx.ui.mobile.form.Button("Tout décocher");
      // bouton.addListener("tap", function(evt) {
      //   this.showTree();
      // }, this);
      // this.getContent().add(bouton);
    },
    /**
     * Build the tree of criteria thanks
     * to the recursive function __affciherNoeuds
     * @param myNewJSON {Object} Object containing the criteres.
     */
    showTree : function(myNewJSON) {
      // var arbre = ifungi.data.Arbre.getInstance();
      // var myNewJSON = arbre.getJsonObject();
      // this.debug(myNewJSON);
      // this.debug(myNewJSON.root.enfants);
      var tabCheckBoxform = [];
      this.getContent().removeAll(tabCheckBoxform);
      var rootEnfants = myNewJSON.root.enfants;
      for (let i = 0; i < rootEnfants.length; i++) {
        // this.getContent().removeAll(checkBoxform);
        var profondeur = 0;
        var checkBoxform = this.__showNodes(myNewJSON, rootEnfants[i], profondeur);
        checkBoxform.checkBoxform.setVisibility("visible");
        // this.debug(checkBoxform);
        tabCheckBoxform.push(checkBoxform)
      }
      // console.log(tab.length);
    },
    /**
     * Displays the critere tree
     * @param myNewJSON {Object} Object containing the criteres.
     * @param node {String} string corresponding to critere.
     * @param profondeur {Integer} Integer corresponding to criterion deepth in JSON tree.
     */
    __showNodes : function(myNewJSON, node, profondeur) {
      if (myNewJSON[node] != null) {
        // var critere = myNewJSON[node].langue.fr;
        var critere = myNewJSON[node].langue;
        var parentNode = myNewJSON[node].parent;
        // var parentCritere = myNewJSON[parentNode].langue.fr;
        var parentCritere = myNewJSON[parentNode].langue;

        var checkBox = new ifungi.ui.GroupCheckBox();

        var form = new qx.ui.mobile.form.Form();
        form.add(checkBox, "&nbsp;&nbsp;&nbsp;&nbsp;".repeat(profondeur) + "&#8226;&nbsp" + critere);
        var checkBoxform = new qx.ui.mobile.form.renderer.Single(form);

        // this.getContent().removeAll(checkBoxform);
        this.getContent().add(checkBoxform);
        // this.debug(critere);

        // this.debug(profondeur);
        // this.debug(checkBoxform);
        checkBoxform.setVisibility("excluded");

        // Display more information about a criterion
        checkBoxform.addListener("longtap", function(evt) {
          // console.log(critere);
          this.fireDataEvent("displayCritereHelp", critere);
        }, this);

        // this.debug(form);
        // this.debug(checkBox);

        if (myNewJSON[node].enfants.length != 0) {
          var checkboxes = [];
          var leafCheckboxes = [];
          // var arr = [];
          // Enfants du noeud
          var nodeEnfants = myNewJSON[node].enfants;

          for (let i = 0; i < nodeEnfants.length; i++) {
            if (myNewJSON[nodeEnfants[i]] != null) {
              if (myNewJSON[nodeEnfants[i]].enfants.length == 0) {
                // this.debug(nodeEnfants[i]);
                leafCheckboxes.push(this.__showNodes(myNewJSON, nodeEnfants[i], profondeur+1));
              } else {
                checkboxes.push(this.__showNodes(myNewJSON, nodeEnfants[i], profondeur+1));
              }
            }
          }
          // Add Listener on checkBoxes
          checkBox.addListener("changeValue", function(evt) {
            // Si le checkBox est coché alors on affiche ses sous Items
            if (evt.getData()) {
              for (let j = 0; j < checkboxes.length; j++) {
                checkboxes[j].checkBoxform.setVisibility("visible");
              }
              // Add JSON tree sheets in a checkBoxGroup and showing their
              var group = new qx.ui.mobile.form.RadioGroup();
              var checkBoxInit = new ifungi.ui.GroupCheckBox();
              group.add(checkBoxInit);
              for (let j = 0; j < leafCheckboxes.length; j++) {
                // this.debug(leafCheckboxes[j]);
                group.add(leafCheckboxes[j].checkBox);
                leafCheckboxes[j].checkBoxform.setVisibility("visible");
              }
            } else {
              // Sinon on n'affiche pas les sous items
              for (let j = 0; j < checkboxes.length; j++) {
                checkboxes[j].checkBoxform.setVisibility("excluded");
                if (checkboxes[j].checkBox._state != undefined) {
                  // this.debug(checkboxes[j].checkBox._state);
                  if (checkboxes[j].checkBox._state == true) {
                    checkboxes[j].checkBox._onTap();
                  }
                }
              }
              for (let j = 0; j < leafCheckboxes.length; j++) {
                leafCheckboxes[j].checkBoxform.setVisibility("excluded");
                // this.debug(leafCheckboxes[j].checkBox._state);
                if (leafCheckboxes[j].checkBox._state == true) {
                  leafCheckboxes[j].checkBox._onTap();
                }
              }
            }
          }, this);
        } else {
          // Add Listener on leaf
          checkBox.addListener("changeValue", function(evt) {
            var parentCritereAndCritere = {};
            parentCritereAndCritere[this.cleanUpSpecialChars(parentCritere)] = this.cleanUpSpecialChars(critere);
            if (evt.getData()) {
              // this.debug(profondeur);
              this.debug(parentCritereAndCritere);
              this.fireDataEvent("ajouterCritere", parentCritereAndCritere);
            } else {
              this.fireDataEvent("enleverCritere", parentCritereAndCritere);
            }
          }, this);
        }
      }
      return {
        checkBoxform,
        checkBox
      };
    },
    /**
     * Application of sensitive case to bind JSON and CSV database
     * strings with special char like accent are replaced by
     * string with none accent.
     * Strings are then put in lowercase for advanced cases where
     * JSON ou CSV database are wrote in lowercase or uppercase.
     * Except columns of images in CSV database because containing
     * names of images corresponding to images in local.
     * @param node {String} string corresponding to a string.
     */
    cleanUpSpecialChars : function(str) {
      var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g // C, c
      ];
      var noaccent = ["A", "a", "E", "e", "I", "i", "O", "o", "U", "u", "N", "n", "C", "c"];
      for (let i = 0; i < accent.length; i++) {
          str = str.replace(accent[i], noaccent[i]);
      }
      return str.toLowerCase();
    }

  }
});
